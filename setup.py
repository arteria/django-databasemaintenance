 
from distutils.core import setup 

setup(
    name='django-databasemaintenance',
    version='0.0.4',
    packages=['databasemaintenance',],
    license='not defined',
    long_description=open('README.txt').read(),
    author='Philippe O. Wagner',
    author_email='wagner@arteria.ch',
)