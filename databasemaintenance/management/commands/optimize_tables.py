from django.core.management.base import NoArgsCommand
from django.conf import settings
 
    
              
class Command(NoArgsCommand):
    help = """ """ 
    def handle_noargs(self, **options):
        from django.db import connection, transaction
        cursor = connection.cursor()
        cursor.execute("SHOW TABLE STATUS WHERE Data_free / Data_length > 0.1")
        #cursor.execute("SHOW TABLE STATUS WHERE Data_free / Data_length > 0.1 AND Data_free > 102400")
        rows = cursor.fetchall()
        for r in rows:
            cursor.execute("OPTIMIZE TABLE "+r[0])
        print "="*30
        print str(len(rows)) + " tables optimized"         
